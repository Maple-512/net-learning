# 功能

## 单点登录（Single Sign On）：IdentityServer

    简称为 SSO。多系统，一次登录

### 想法

1. 使用JWT，不用Cookie（应该说，更能理解）
   1. 研究`Microsoft.AspNetCore.Authentication.JwtBearer`源码后，发现JWT一经创建，在过期时间前，要让它失效的话比较客困难。所以，JWT一般用于API访问授权，但安全起见，一般过期时间较短。

2. Cookie
   1. 问题：
      1. 设置自定义登录地址时，无效