# RESTful API

* API（Application Programming Interface）

    是应用之间的一种交流方式

* REST API（Representational State Transfer Application Programming Interface）

    是API的一种架构风格，专为WEB应用设计，即：REST API是Web API设计的一种规范

  * REST（Representational State Transfer 表现层状态转化）：互联网软件的架构原则

  * RESTful：是REST的一种具体实现方式，但RESTful并未完全实现REST的设计原则

## 关键字

* 资源（Resource）：

    REST中的“**表现层**”指的是“**资源**”的“**表现层**”。资源，就是网络上的一个实体（具体信息）。可以是一段文本，图片，服务。可以用URI指向它，每种资源对应一个特定的URI。要获取这个资源，访问它的URI就可以了。

* 表现层（Representation）
  
    把“资源”具体呈现出来的方式就是表现层。比如：文本用txt格式，也可以用html格式、xml格式等

* 状态转化（State Transfer）
  
    访问一个网站，就代表了客户端和服务器的一个互动过程。在这个过程中，势必涉及到数据和状态的变化。
    互联网的通信协议HTTP，是一个无状态的协议。这意味着，所有的状态都保存在服务器端。因此，**如果客户端想要操作服务器，必须通过某种手段，让服务器发生“状态转化”（Sate Transfer）。而这种转化是建立在表现层之上的。**所以，就是“（**资源**的）表现层状态转化”。

* 综上：
  * 每一个URI都代表一种资源
  * 客户端和服务器之间，传递这种资源的某种表现层
  * 客户端通过四个HTTP动词，对服务器资源进行操作，实现“表现层状态转化”

* 误区：
  * URI包含动词
  
    因为“资源”表示一种实体，所以应该是名词，URI不应该有动词，动词应该放在HTTP协议中。
    如：URI：`/posts/show/1`，其中`show`是动词，这个URI设计就错了，应该是：`/posts/1`，然后用`get`表示`show`

  * URI中加入版本号：`www.example.com/app/1.0/foo`
  
    因为不同的版本，可以理解成：同一种资源的不同表现形式，所以应该采用同一个URI。版本号可以在HTTP请求头信息的Accept中进行区分（见[Versioning REST Service](http://www.informit.com/articles/article.aspx?p=1566460))，如：`Accept: vnd.example-com.foo+json;version=1.0`

## 参考

* [阮一峰：理解RESTful架构](http://www.ruanyifeng.com/blog/2011/09/restful.html)



