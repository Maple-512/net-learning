namespace Shared.Test.Localization
{
    using Abp.Configuration.Startup;
    using Abp.Localization.Dictionaries;
    using Abp.Reflection.Extensions;
    using Shared.Localization.Yml;

    public class LocalizationConfigurer
    {
        public const string LocalizationSourceName = "YmlTest";

        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(LocalizationSourceName,
                    new YmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(LocalizationConfigurer).GetAssembly(),
                        "Shared.Test.Localization.SourceFiles"
                    )
                )
            );
        }
    }
}
