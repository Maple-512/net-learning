namespace Shared.Test.Localization
{
    using Abp.Localization;
    using Abp.TestBase;
    using Shouldly;
    using Xunit;

    public class MainTest : AbpIntegratedTestBase<LocalizationTestModule>
    {
        private readonly ILocalizationManager _localizationManager;

        public MainTest()
        {
            _localizationManager = Resolve<ILocalizationManager>();
        }

        [Fact]
        public void Yaml_Test()
        {
            using (CultureInfoHelper.Use("en"))
            {
                var source = _localizationManager.GetSource(LocalizationConfigurer.LocalizationSourceName);

                source.GetString("Test").ShouldBe("Test");
                source.GetString("Welcome").ShouldBe("Welcome to NetLearning");
            }

            using (CultureInfoHelper.Use("zh-CN"))
            {
                var source = _localizationManager.GetSource(LocalizationConfigurer.LocalizationSourceName);

                source.GetString("Test").ShouldBe("测试");
                source.GetString("Welcome").ShouldBe("欢迎来到 NetLearning");
            }
        }
    }
}
