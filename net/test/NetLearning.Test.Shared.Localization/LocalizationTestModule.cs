namespace Shared.Test.Localization
{
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Abp.TestBase;

    [DependsOn(
        typeof(AbpTestBaseModule)
        )]
    public class LocalizationTestModule : AbpModule
    {
        public override void PreInitialize()
        {
            LocalizationConfigurer.Configure(Configuration.Localization);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(LocalizationTestModule).GetAssembly());
        }
    }
}
