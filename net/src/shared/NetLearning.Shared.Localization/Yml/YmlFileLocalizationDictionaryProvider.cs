namespace Shared.Localization.Yml
{
    using System.IO;
    using Abp.Localization.Dictionaries;

    public class YmlFileLocalizationDictionaryProvider : LocalizationDictionaryProviderBase
    {
        private readonly string _directoryPath;

        public YmlFileLocalizationDictionaryProvider(string directoryPath)
        {
            _directoryPath = directoryPath;
        }

        protected override void InitializeDictionaries()
        {
            var fileNames = Directory.GetFiles(_directoryPath, "*.yml", SearchOption.TopDirectoryOnly);

            foreach (var fileName in fileNames)
            {
                InitializeDictionary(CreateJsonLocalizationDictionary(fileName), isDefault: fileName.EndsWith(SourceName + ".yml"));
            }
        }

        protected virtual YmlLocalizationDictionary CreateJsonLocalizationDictionary(string fileName)
        {
            return YmlLocalizationDictionary.BuildFromFile(fileName);
        }
    }
}
