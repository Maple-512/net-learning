namespace Shared.Localization.Yml
{
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using Abp.Localization.Dictionaries;

    public class YmlEmbeddedFileLocalizationDictionaryProvider : LocalizationDictionaryProviderBase
    {
        private readonly Assembly _assembly;

        private readonly string _rootNamespace;

        public YmlEmbeddedFileLocalizationDictionaryProvider(Assembly assembly, string rootNamespace)
        {
            _assembly = assembly;
            _rootNamespace = rootNamespace;
        }

        protected override void InitializeDictionaries()
        {
            var allCultureInfos = CultureInfo.GetCultures(CultureTypes.AllCultures);

            var resourceNames = _assembly.GetManifestResourceNames().Where(resouceName =>
                allCultureInfos.Any(culture =>
                resouceName.EndsWith($"{SourceName}.yml", true, null)
                || resouceName.EndsWith($"{SourceName}-{culture.Name}.yml"
                    , true
                    , null)
                )).ToList();

            foreach (var resourceName in resourceNames)
            {
                if (resourceName.StartsWith(_rootNamespace))
                {
                    using var stream = _assembly.GetManifestResourceStream(resourceName);

                    var jsonString = Utf8Helper.ReadStringFromStream(stream);

                    InitializeDictionary(CreateJsonLocalizationDictionary(jsonString), isDefault: resourceName.EndsWith($"{SourceName}.yml"));
                }
            }
        }

        protected virtual YmlLocalizationDictionary CreateJsonLocalizationDictionary(string yamlString)
        {
            return YmlLocalizationDictionary.BuildFromYamlString(yamlString);
        }
    }
}
