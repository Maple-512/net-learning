namespace Shared.Localization.Yml
{
    using System.Collections.Generic;

    public class YmlLocalizationFile
    {
        public YmlLocalizationFile()
        {
            Texts = new Dictionary<string, string>();
        }

        /// <summary>
        /// 区域名 eg : en , en-us, zh-CN
        /// </summary>
        public string Culture { get; set; }

        public Dictionary<string, string> Texts { get; }
    }
}
