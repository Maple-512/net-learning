namespace Shared.Localization.Yml
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using Abp;
    using Abp.Collections.Extensions;
    using Abp.Extensions;
    using Abp.Localization.Dictionaries;
    using YamlDotNet.Core;
    using YamlDotNet.Serialization;

    public class YmlLocalizationDictionary : LocalizationDictionary
    {
        public YmlLocalizationDictionary(CultureInfo cultureInfo) : base(cultureInfo)
        {
        }

        public static YmlLocalizationDictionary BuildFromFile(string filePath)
        {
            try
            {
                return BuildFromYamlString(File.ReadAllText(filePath));
            }
            catch (Exception ex)
            {
                throw new AbpException("Invalid localization file format! " + filePath, ex);
            }
        }

        /// <summary>
        ///     Builds an <see cref="JsonLocalizationDictionary" /> from given json string.
        /// </summary>
        /// <param name="yamlString">Json string</param>
        public static YmlLocalizationDictionary BuildFromYamlString(string yamlString)
        {
            YmlLocalizationFile yamlFile;

            try
            {
                var deserializere = new Deserializer();

                yamlFile = deserializere.Deserialize<YmlLocalizationFile>(yamlString);
            }
            catch (YamlException ex)
            {
                throw new AbpException("Can not parse json string. " + ex.Message);
            }

            var cultureCode = yamlFile.Culture;

            if (string.IsNullOrEmpty(cultureCode))
            {
                throw new AbpException("Culture is empty in language yaml file.");
            }

            var dictionary = new YmlLocalizationDictionary(CultureInfo.GetCultureInfo(cultureCode));

            var dublicateNames = new List<string>();

            foreach (var item in yamlFile.Texts)
            {
                if (string.IsNullOrEmpty(item.Key))
                {
                    throw new AbpException("The key is empty in given yaml string.");
                }

                if (dictionary.Contains(item.Key))
                {
                    dublicateNames.Add(item.Key);
                }

                dictionary[item.Key] = item.Value.NormalizeLineEndings();
            }

            if (dublicateNames.Count > 0)
            {
                throw new AbpException(
                    "A dictionary can not contain same key twice. There are some duplicated names: " +
                    dublicateNames.JoinAsString(", "));
            }

            return dictionary;
        }
    }
}
