namespace Shared.Nlog
{
    using Castle.Facilities.Logging;

    public static class LoggingFacilityExtension
    {
        public static LoggingFacility UseNLog(this LoggingFacility loggingFacility)
        {
            return loggingFacility.LogUsing<NLogLoggerFactory>();
        }
    }
}
