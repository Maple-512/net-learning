namespace Shared.Nlog
{
    using System;
    using System.Globalization;
    using ILogger = Castle.Core.Logging.ILogger;
    using NLogCore = NLog;

    [Serializable]
    public class NLogLogger : MarshalByRefObject, ILogger
    {
        protected internal NLogCore.ILogger Logger { get; set; }

        public NLogLogger(NLogCore.ILogger logger)
        {
            Logger = logger;
        }

        internal NLogLogger()
        {
        }

        #region Debug

        public bool IsDebugEnabled => Logger.IsEnabled(NLogCore.LogLevel.Debug);

        public void Debug(string message)
        {
            Logger.Debug(message);
        }

        public void Debug(Func<string> messageFactory)
        {
            Logger.Debug(messageFactory);
        }

        public void Debug(string message, Exception exception)
        {
            Logger.Debug(exception, message);
        }

        public void DebugFormat(string format, params object[] args)
        {
            Logger.Debug(CultureInfo.InvariantCulture, format, args);
        }

        public void DebugFormat(Exception exception, string format, params object[] args)
        {
            Logger.Debug(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void DebugFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Debug(formatProvider, format, args);
        }

        public void DebugFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Debug(exception, formatProvider, format, args);
        }

        #endregion

        #region Error

        public bool IsErrorEnabled => Logger.IsEnabled(NLogCore.LogLevel.Error);

        public void Error(string message)
        {
            Logger.Error(message);
        }

        public void Error(Func<string> messageFactory)
        {
            Logger.Error(messageFactory);
        }

        public void Error(string message, Exception exception)
        {
            Logger.Error(exception, message);
        }

        public void ErrorFormat(string format, params object[] args)
        {
            Logger.Error(CultureInfo.InvariantCulture, format, args);
        }

        public void ErrorFormat(Exception exception, string format, params object[] args)
        {
            Logger.Error(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void ErrorFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Error(formatProvider, format, args);
        }

        public void ErrorFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Error(exception, formatProvider, format, args);
        }

        #endregion

        #region Fatal

        public bool IsFatalEnabled => Logger.IsEnabled(NLogCore.LogLevel.Fatal);

        public void Fatal(string message)
        {
            Logger.Fatal(message);
        }

        public void Fatal(Func<string> messageFactory)
        {
            Logger.Fatal(messageFactory);
        }

        public void Fatal(string message, Exception exception)
        {
            Logger.Fatal(exception, message);
        }

        public void FatalFormat(string format, params object[] args)
        {
            Logger.Fatal(CultureInfo.InvariantCulture, format, args);
        }

        public void FatalFormat(Exception exception, string format, params object[] args)
        {
            Logger.Fatal(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void FatalFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Fatal(formatProvider, format, args);
        }

        public void FatalFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Fatal(exception, formatProvider, format, args);
        }

        #endregion

        #region Info

        public bool IsInfoEnabled => Logger.IsEnabled(NLogCore.LogLevel.Info);

        public void Info(string message)
        {
            Logger.Info(message);
        }

        public void Info(Func<string> messageFactory)
        {
            Logger.Info(messageFactory);
        }

        public void Info(string message, Exception exception)
        {
            Logger.Info(exception, message);
        }

        public void InfoFormat(string format, params object[] args)
        {
            Logger.Info(CultureInfo.InvariantCulture, format, args);
        }

        public void InfoFormat(Exception exception, string format, params object[] args)
        {
            Logger.Info(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void InfoFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Info(formatProvider, format, args);
        }

        public void InfoFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Info(exception, formatProvider, format, args);
        }

        #endregion

        #region Warn

        public bool IsWarnEnabled => Logger.IsEnabled(NLogCore.LogLevel.Warn);

        public void Warn(string message)
        {
            Logger.Warn(message);
        }

        public void Warn(Func<string> messageFactory)
        {
            Logger.Warn(messageFactory);
        }

        public void Warn(string message, Exception exception)
        {
            Logger.Warn(exception, message);
        }

        public void WarnFormat(string format, params object[] args)
        {
            Logger.Warn(CultureInfo.InvariantCulture, format, args);
        }

        public void WarnFormat(Exception exception, string format, params object[] args)
        {
            Logger.Warn(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void WarnFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Warn(formatProvider, format, args);
        }

        public void WarnFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Warn(exception, formatProvider, format, args);
        }

        #endregion

        #region Trace

        public bool IsTraceEnabled => Logger.IsEnabled(NLogCore.LogLevel.Trace);

        public void Trace(string message)
        {
            Logger.Trace(message);
        }

        public void Trace(Func<string> messageFactory)
        {
            Logger.Trace(messageFactory);
        }

        public void Trace(string message, Exception exception)
        {
            Logger.Trace(exception, message);
        }

        public void TraceFormat(string format, params object[] args)
        {
            Logger.Trace(format, args);
        }

        public void TraceFormat(Exception exception, string format, params object[] args)
        {
            Logger.Trace(exception, CultureInfo.InvariantCulture, format, args);
        }

        public void TraceFormat(IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Trace(formatProvider, format, args);
        }

        public void TraceFormat(Exception exception, IFormatProvider formatProvider, string format, params object[] args)
        {
            Logger.Trace(exception, formatProvider, format, args);
        }

        #endregion

        public ILogger CreateChildLogger(string loggerName)
        {
            return new NLogLogger(NLogCore.LogManager.GetLogger(Logger.Name + "." + loggerName));
        }
    }
}
