namespace Domain.Core.Blogs.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Configurations;
    using Infra.Common.Constants;

    [Table(nameof(Blog), Schema = TableSchemas.Blog)]
    public class Blog : FullAuditedAggregateRoot<long>
    {
        public long ArticleId { get; set; }

        #region Property

        /// <summary>
        /// 类型
        /// </summary>
        public BlogTypeEnum BlogType { get; set; }

        /// <summary>
        /// 阅读
        /// </summary>
        public int ReadingNumber { get; private set; }

        /// <summary>
        /// 赞同
        /// </summary>
        public int DiggNumber { get; private set; }

        /// <summary>
        /// 反对
        /// </summary>
        public int BuryNumber { get; private set; }

        /// <summary>
        /// 评论
        /// </summary>
        public int CommentNumber { get; private set; }

        #endregion

        [ForeignKey(nameof(ArticleId))]
        public virtual Article Article { get; set; }

        [ForeignKey("BlogId")]
        public virtual ICollection<Comment> Comments { get; set; }

        #region Event

        /// <summary>
        /// 阅读
        /// </summary>
        internal void Reading()
        {
            ++ReadingNumber;
        }

        /// <summary>
        /// 赞同
        /// </summary>
        internal void AddDigg()
        {
            ++DiggNumber;
        }

        /// <summary>
        /// 取消赞同
        /// </summary>
        internal void RemoveDigg()
        {
            if (DiggNumber > 1)
            {
                --DiggNumber;
            }
        }

        /// <summary>
        /// 反对
        /// </summary>
        internal void AddBury()
        {
            ++BuryNumber;
        }

        /// <summary>
        /// 取消反对
        /// </summary>
        internal void RemoveBury()
        {
            if (BuryNumber > 1)
            {
                --BuryNumber;
            }
        }

        /// <summary>
        /// 评论
        /// </summary>
        internal void AddComment()
        {
            ++CommentNumber;
        }

        /// <summary>
        /// 取消评论
        /// </summary>
        internal void RemoveComment()
        {
            if (CommentNumber > 1)
            {
                --CommentNumber;
            }
        }

        #endregion
    }
}
