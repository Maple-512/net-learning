namespace Domain.Core.Blogs.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities.Auditing;
    using Configurations;
    using Identities.Entities;
    using Infra.Common.Constants;

    /// <summary>
    /// 评论
    /// </summary>
    [Table(nameof(Comment), Schema = TableSchemas.Blog)]
    public class Comment : AuditedEntity<long>
    {
        /// <summary>
        /// Construct
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="blogId"></param>
        /// <param name="content"></param>
        /// <param name="commentType"></param>
        public Comment(long? parentId, long blogId, string content, CommentTypeEnum commentType)
        {
            ParentId = parentId;
            BlogId = blogId;
            Content = content;
            CommentType = commentType;
        }

        public long? ParentId { get; }

        public long BlogId { get; }

        public string Content { get; }

        public CommentTypeEnum CommentType { get; }

        [ForeignKey(nameof(CreatorUserId))]
        public virtual User CreatorUser { get; set; }

        [ForeignKey(nameof(LastModifierUserId))]
        public virtual User LastModifierUser { get; set; }

        [ForeignKey(nameof(ParentId))]
        public virtual ICollection<Comment> Comments { get; set; }
    }
}
