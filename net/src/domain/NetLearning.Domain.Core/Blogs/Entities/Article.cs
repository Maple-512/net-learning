namespace Domain.Core.Blogs.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;
    using Configurations;

    /// <summary>
    /// 文章
    /// </summary>
    [Table(nameof(Article), Schema = TableSchemas.Blog)]
    public class Article : Entity<long>
    {
        public const int MaxTitleLength = 64;

        public const int SummarySubLength = 50;

        public Article(string title, string content)
        {
            Title = title;
            Content = content;
            Summary = Content.Substring(0, SummarySubLength);
        }

        [Required]
        [StringLength(MaxTitleLength)]
        public string Title { get; }

        [Required]
        [Column(TypeName = "text")]
        public string Content { get; }

        [StringLength(SummarySubLength)]
        public string Summary { get; }
    }
}
