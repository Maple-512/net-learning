namespace Domain.Core.Blogs.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;
    using Domain.Core.Configurations;

    [Table(nameof(BlogTopic), Schema = TableSchemas.Blog)]
    public class BlogTopic : Entity<long>
    {
        public long BlogId { get; set; }

        [ForeignKey(nameof(BlogId))]
        public virtual Blog Blog { get; set; }

        public long TopicId { get; set; }

        [ForeignKey(nameof(TopicId))]
        public virtual Topic Topic { get; set; }
    }
}
