namespace Domain.Core.Blogs.Entities
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;
    using Configurations;

    /// <summary>
    /// 主题（分类）
    /// </summary>
    [Table(nameof(Topic), Schema = TableSchemas.Blog)]
    public class Topic : Entity<long>
    {
        public const int MaxTitleLength = 16;

        public const int MaxDescriptionLength = 256;

        public Topic(string title, string description)
        {
            Title = title;
            Description = description;
        }

        [Required]
        [StringLength(MaxTitleLength)]
        public string Title { get; }

        [StringLength(MaxDescriptionLength)]
        public string Description { get; }
    }
}
