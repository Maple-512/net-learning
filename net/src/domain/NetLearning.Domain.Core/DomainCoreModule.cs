namespace Domain.Core
{
    using Abp.MailKit;
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Abp.Zero;
    using Abp.Zero.Configuration;
    using Domain.Core.Identities.Entities;
    using Infra.Common;

    [DependsOn(
        typeof(InfraCommonModule)
        , typeof(AbpZeroCoreModule)
        , typeof(AbpMailKitModule)
        )]
    public class DomainCoreModule : AbpModule
    {
        public override void PreInitialize()
        {
            // 启用审计日志是否包括未登录用户
            Configuration.Auditing.IsEnabledForAnonymousUsers = true;

            // 定义实体类型
            Configuration.Modules.Zero().EntityTypes.Tenant = typeof(Tenant);
            Configuration.Modules.Zero().EntityTypes.Role = typeof(Role);
            Configuration.Modules.Zero().EntityTypes.User = typeof(User);

            // 启用多租户
            Configuration.MultiTenancy.IsEnabled = InfraCommonConfig.MultiTenancyEnabled;

            // 配置管理员角色
            Role.StaticRolesConfigure(Configuration.Modules.Zero().RoleManagement);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DomainCoreModule).GetAssembly());
        }
    }
}
