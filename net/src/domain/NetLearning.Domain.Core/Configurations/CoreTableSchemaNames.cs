namespace Domain.Core.Configurations
{
    public static class TableSchemas
    {
        public const string Common = "Common";

        public const string Identity = "Identity";

        public const string CastleLog = "CastleLog";

        public const string BackJob = "BackJob";

        public const string Blog = "Blog";
    }
}
