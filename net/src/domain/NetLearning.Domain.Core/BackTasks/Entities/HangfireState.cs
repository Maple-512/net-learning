﻿namespace Domain.Core.BackTasks.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireState : IEntity<long>
    {
        public long Id { get; set; }

        public long JobId { get; set; }

        public virtual HangfireJob Job { get; set; }

        public string Name { get; set; }

        public string Reason { get; set; }

        public DateTime CreatedAt { get; set; }

        public string Data { get; set; }

        public bool IsTransient()
        {
            throw new NotImplementedException();
        }
    }
}
