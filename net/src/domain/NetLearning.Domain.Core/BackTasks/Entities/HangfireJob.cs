namespace Domain.Core.BackTasks.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireJob : IEntity<long>
    {
        public const string TableName = "Job";

        public long StateId { get; set; }

        public string StateName { get; set; }

        public string InvocationData { get; set; }

        public string Arguments { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ExpireAt { get; set; }

        public long Id { get; set; }

        public bool IsTransient()
        {
            throw new NotImplementedException();
        }
    }
}
