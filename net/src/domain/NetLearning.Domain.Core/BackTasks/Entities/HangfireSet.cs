﻿namespace Domain.Core.BackTasks.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireSet : IEntity<long>
    {
        public string Key { get; set; }

        public float Score { get; set; }

        public string Value { get; set; }

        public DateTime ExpireAt { get; set; }

        public long Id { get; set; }

        public bool IsTransient()
        {
            throw new NotImplementedException();
        }
    }
}
