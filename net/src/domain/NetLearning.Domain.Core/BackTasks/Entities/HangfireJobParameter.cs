﻿namespace Domain.Core.BackTasks.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireJobParameter : IEntity<long>
    {
        public long JobId { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public long Id { get; set; }

        public bool IsTransient()
        {
            throw new System.NotImplementedException();
        }
    }
}
