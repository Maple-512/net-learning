﻿namespace Domain.Core.BackTasks.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireJobQueue : IEntity
    {
        public int Id { get; set; }

        public string Queue { get; set; }

        public long JobId { get; set; }

        public DateTime FetchedAt { get; set; }

        public bool IsTransient()
        {
            throw new NotImplementedException();
        }
    }
}
