﻿namespace Domain.Core.BackTasks.Entities
{
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Domain.Entities;

    [NotMapped]
    public class HangfireSchema : IEntity<long>
    {
        public int Version { get; set; }

        public long Id { get; set; }

        public bool IsTransient()
        {
            throw new System.NotImplementedException();
        }
    }
}
