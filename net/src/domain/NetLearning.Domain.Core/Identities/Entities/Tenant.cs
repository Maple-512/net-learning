namespace Domain.Core.Identities.Entities
{
    using Abp.MultiTenancy;

    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}
