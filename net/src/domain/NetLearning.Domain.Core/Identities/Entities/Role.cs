namespace Domain.Core.Identities.Entities
{
    using System.ComponentModel.DataAnnotations;
    using Abp.Authorization.Roles;
    using Abp.MultiTenancy;
    using Abp.Zero.Configuration;

    public class Role : AbpRole<User>
    {
        public const int MaxDescriptionLength = 32;

        public const string HostAdminRoleName = "Host Admin";

        public const string TenantAdminRoleName = "Tenant Admin";

        public Role()
        {
        }

        public Role(int? tenantId, string displayName)
            : base(tenantId, displayName)
        {
        }

        public Role(int? tenantId, string name, string displayName)
            : base(tenantId, name, displayName)
        {
        }

        [StringLength(MaxDescriptionLength)]
        public string Description { get; set; }

        public static void StaticRolesConfigure(IRoleManagementConfig config)
        {
            // Static host roles
            config.StaticRoles.Add(
                new StaticRoleDefinition(
                    Role.HostAdminRoleName,
                    MultiTenancySides.Host
                )
            );

            // Static tenant roles
            config.StaticRoles.Add(
                new StaticRoleDefinition(
                    Role.TenantAdminRoleName,
                    MultiTenancySides.Tenant
                )
            );
        }
    }
}
