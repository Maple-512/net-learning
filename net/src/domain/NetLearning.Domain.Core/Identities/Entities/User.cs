namespace Domain.Core.Identities.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using Abp.Authorization.Users;
    using Abp.Extensions;

    public class User : AbpUser<User>
    {
        public const string DefaultPassword = "123qwe";

        [NotMapped]
        [Obsolete]
        public new string Surname { get; set; }

        [NotMapped]
        [Obsolete]
        public new string FullName { get; }

        public static string CreateRandomPassword()
        {
            return Guid.NewGuid().ToString("N").Truncate(16);
        }

        public static User CreateTenantAdminUser(int tenantId, string emailAddress)
        {
            var user = new User
            {
                TenantId = tenantId,
                UserName = AdminUserName,
                Name = AdminUserName,
                EmailAddress = emailAddress,
                Roles = new List<UserRole>()
            };

            user.SetNormalizedNames();

            return user;
        }
    }
}
