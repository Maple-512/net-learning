namespace Domain.Core.Identities
{
    using Abp.Authorization;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Options;
    using Domain.Core.Identities.Entities;
    using Domain.Core.Identities.Services;

    public class UserClaimsPrincipalFactory : AbpUserClaimsPrincipalFactory<User, Role>
    {
        public UserClaimsPrincipalFactory(
            UserManager userManager,
            RoleManager roleManager,
            IOptions<IdentityOptions> optionsAccessor)
            : base(
                  userManager,
                  roleManager,
                  optionsAccessor)
        {
        }
    }
}
