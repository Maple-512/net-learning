﻿namespace Domain.Core.Identities.Services
{
    using Abp.Authorization;
    using Entities;

    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {
        }
    }
}
