namespace Infra.Data.EntityFrameworkCore.Extensions
{
    using Abp.Application.Editions;
    using Abp.Application.Features;
    using Abp.Auditing;
    using Abp.Authorization;
    using Abp.Authorization.Roles;
    using Abp.Authorization.Users;
    using Abp.BackgroundJobs;
    using Abp.Configuration;
    using Abp.EntityHistory;
    using Abp.Localization;
    using Abp.Notifications;
    using Abp.Organizations;
    using Domain.Core.Configurations;
    using Domain.Core.Identities.Entities;
    using Microsoft.EntityFrameworkCore;

    public static class DbModelBuilderExtension
    {
        public static void ChangeAbpTable(this ModelBuilder modelBuilder)
        {
            #region Identity

            modelBuilder.SetTableName<OrganizationUnit>(nameof(OrganizationUnit), TableSchemas.Identity);
            modelBuilder.SetTableName<PermissionSetting>(nameof(PermissionSetting), TableSchemas.Identity);
            modelBuilder.SetTableName<Role>(nameof(Role), TableSchemas.Identity);
            modelBuilder.SetTableName<UserOrganizationUnit>(nameof(UserOrganizationUnit), TableSchemas.Identity);
            modelBuilder.SetTableName<OrganizationUnitRole>(nameof(OrganizationUnitRole), TableSchemas.Identity);
            modelBuilder.SetTableName<UserRole>(nameof(UserRole), TableSchemas.Identity);
            modelBuilder.SetTableName<User>(nameof(User), TableSchemas.Identity);
            modelBuilder.SetTableName<UserAccount>(nameof(UserAccount), TableSchemas.Identity);
            modelBuilder.SetTableName<UserClaim>(nameof(UserClaim), TableSchemas.Identity);
            modelBuilder.SetTableName<RoleClaim>(nameof(RoleClaim), TableSchemas.Identity);
            modelBuilder.SetTableName<UserToken>(nameof(UserToken), TableSchemas.Identity);
            modelBuilder.SetTableName<Tenant>(nameof(Tenant), TableSchemas.Identity);
            modelBuilder.SetTableName<UserLogin>(nameof(UserLogin), TableSchemas.Identity);
            modelBuilder.SetTableName<UserLoginAttempt>(nameof(UserLoginAttempt), TableSchemas.Identity);
            #endregion

            #region Common

            modelBuilder.SetTableName<Edition>(nameof(Edition), TableSchemas.Common);
            modelBuilder.SetTableName<EntityChange>(nameof(EntityChange), TableSchemas.Common);
            modelBuilder.SetTableName<EntityChangeSet>(nameof(EntityChangeSet), TableSchemas.Common);
            modelBuilder.SetTableName<EntityPropertyChange>(nameof(EntityPropertyChange), TableSchemas.Common);
            modelBuilder.SetTableName<FeatureSetting>(nameof(FeatureSetting), TableSchemas.Common);
            modelBuilder.SetTableName<ApplicationLanguage>(nameof(ApplicationLanguage), TableSchemas.Common);
            modelBuilder.SetTableName<ApplicationLanguageText>(nameof(ApplicationLanguageText), TableSchemas.Common);
            modelBuilder.SetTableName<NotificationInfo>(nameof(NotificationInfo), TableSchemas.Common);
            modelBuilder.SetTableName<NotificationSubscriptionInfo>(nameof(NotificationSubscriptionInfo), TableSchemas.Common);
            modelBuilder.SetTableName<Setting>(nameof(Setting), TableSchemas.Common);
            modelBuilder.SetTableName<TenantNotificationInfo>(nameof(TenantNotificationInfo), TableSchemas.Common);
            modelBuilder.SetTableName<UserNotificationInfo>(nameof(UserNotificationInfo), TableSchemas.Common);

            #endregion

            #region CastleLog

            modelBuilder.SetTableName<AuditLog>(nameof(AuditLog), TableSchemas.CastleLog);

            #endregion

            #region BackJob

            modelBuilder.SetTableName<BackgroundJobInfo>(nameof(BackgroundJobInfo), TableSchemas.BackJob);

            #endregion
        }

        internal static void SetTableName<TEntity>(this ModelBuilder modelBuilder, string tableName, string schemaName) where TEntity : class
        {
            if (schemaName == null)
            {
                modelBuilder.Entity<TEntity>().ToTable(tableName);
            }
            else
            {
                modelBuilder.Entity<TEntity>().ToTable(tableName, schemaName);
            }
        }
    }
}
