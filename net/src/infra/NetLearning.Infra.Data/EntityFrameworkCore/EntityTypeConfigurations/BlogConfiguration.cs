namespace Infra.Data.EntityFrameworkCore.EntityTypeConfigurations
{
    using Domain.Core.Blogs.Entities;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    internal static class BlogConfiguration
    {
        internal static void ApplyBlogConfiguration(this ModelBuilder builder)
        {
            builder.ApplyConfiguration(new BlogConfig());
            builder.ApplyConfiguration(new ArticleConfig());
            builder.ApplyConfiguration(new TopicConfig());
            builder.ApplyConfiguration(new CommentConfig());
        }
    }

    internal class BlogConfig : IEntityTypeConfiguration<Blog>
    {
        public void Configure(EntityTypeBuilder<Blog> builder)
        {
            builder.HasIndex(m => m.CreationTime);

            builder.HasIndex(m => new { m.BuryNumber, m.CommentNumber, m.DiggNumber, m.ReadingNumber });
        }
    }

    internal class ArticleConfig : IEntityTypeConfiguration<Article>
    {
        public void Configure(EntityTypeBuilder<Article> builder)
        {
            builder.HasIndex(m => m.Title);
        }
    }

    internal class TopicConfig : IEntityTypeConfiguration<Topic>
    {
        public void Configure(EntityTypeBuilder<Topic> builder)
        {
            builder.HasIndex(m => m.Title);
        }
    }

    internal class CommentConfig : IEntityTypeConfiguration<Comment>
    {
        public void Configure(EntityTypeBuilder<Comment> builder)
        {
            builder.HasIndex(m => m.CreationTime);
        }
    }
}
