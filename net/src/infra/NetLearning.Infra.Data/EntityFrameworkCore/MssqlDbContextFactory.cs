namespace Infra.Data.EntityFrameworkCore
{
    using Common.Helpers.Web;
    using Domain.Core.Configurations;
    using Infra.Common;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Design;
    using Microsoft.Extensions.Configuration;

    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class MssqlDbContextFactory : IDesignTimeDbContextFactory<MssqlDbContext>
    {
        public MssqlDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<MssqlDbContext>();
            var configuration = AppConfiguration.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            MssqlDbContextConfigurer.Configure(builder, configuration.GetConnectionString(InfraCommonConfig.ConnectionStringName));

            return new MssqlDbContext(builder.Options);
        }
    }
}
