namespace Infra.Data.EntityFrameworkCore.Seed.Tenants
{
    using System.Linq;
    using Abp.MultiTenancy;
    using Domain.Core.Identities.Entities;
    using Domain.Core.Identities.Services;
    using Microsoft.EntityFrameworkCore;

    public class DefaultTenantBuilder
    {
        private readonly MssqlDbContext _context;

        public DefaultTenantBuilder(MssqlDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateDefaultTenant();
        }

        private void CreateDefaultTenant()
        {
            // Default tenant

            var defaultTenant = _context.Tenants.IgnoreQueryFilters().FirstOrDefault(t => t.TenancyName == AbpTenantBase.DefaultTenantName);
            if (defaultTenant == null)
            {
                defaultTenant = new Tenant(AbpTenantBase.DefaultTenantName, AbpTenantBase.DefaultTenantName);

                var defaultEdition = _context.Editions.IgnoreQueryFilters().FirstOrDefault(e => e.Name == EditionManager.DefaultEditionName);

                if (defaultEdition != null)
                {
                    defaultTenant.EditionId = defaultEdition.Id;
                }

                _context.Tenants.Add(defaultTenant);

                _context.SaveChanges();
            }
        }
    }
}
