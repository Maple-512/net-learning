namespace Infra.Data.EntityFrameworkCore.Seed.Host
{
    using System.Collections.Generic;
    using System.Linq;
    using Abp.Localization;
    using Abp.MultiTenancy;
    using Common;
    using Microsoft.EntityFrameworkCore;

    public class DefaultLanguagesCreator
    {
        public static List<ApplicationLanguage> InitialLanguages => GetInitialLanguages();

        private readonly MssqlDbContext _context;

        private static List<ApplicationLanguage> GetInitialLanguages()
        {
            var tenantId = InfraCommonConfig.MultiTenancyEnabled ? null : (int?)MultiTenancyConsts.DefaultTenantId;

            return new List<ApplicationLanguage>
            {
                new ApplicationLanguage(tenantId, "en", "English"),
                new ApplicationLanguage(tenantId, "zh-CN", "简体中文"),
            };
        }

        public DefaultLanguagesCreator(MssqlDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateLanguages();
        }

        private void CreateLanguages()
        {
            foreach (var language in InitialLanguages)
            {
                AddLanguageIfNotExists(language);
            }
        }

        private void AddLanguageIfNotExists(ApplicationLanguage language)
        {
            if (_context.Languages.IgnoreQueryFilters().Any(l => l.TenantId == language.TenantId && l.Name == language.Name))
            {
                return;
            }

            _context.Languages.Add(language);

            _context.SaveChanges();
        }
    }
}
