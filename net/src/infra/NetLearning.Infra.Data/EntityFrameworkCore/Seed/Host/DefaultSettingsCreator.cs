namespace Infra.Data.EntityFrameworkCore.Seed.Host
{
    using System.Linq;
    using Abp.Configuration;
    using Abp.Localization;
    using Abp.MultiTenancy;
    using Abp.Net.Mail;
    using Common;
    using Microsoft.EntityFrameworkCore;

    public class DefaultSettingsCreator
    {
        private readonly MssqlDbContext _context;

        public DefaultSettingsCreator(MssqlDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            int? tenantId = null;

            if (InfraCommonConfig.MultiTenancyEnabled == false)
            {
                tenantId = MultiTenancyConsts.DefaultTenantId;
            }

            // Emailing
            AddSettingIfNotExists(EmailSettingNames.DefaultFromAddress, "17376589512@163.com", tenantId);
            AddSettingIfNotExists(EmailSettingNames.DefaultFromDisplayName, "NetLearning.Infra.com mailer", tenantId);

            //AddSettingIfNotExists(EmailSettingNames.Smtp.UserName, "17376589512@163.com", tenantId);
            //AddSettingIfNotExists(EmailSettingNames.Smtp.Password, "51793ed4", tenantId);
            //AddSettingIfNotExists(EmailSettingNames.Smtp.Host, "smtp.163.com", tenantId);
            //AddSettingIfNotExists(EmailSettingNames.Smtp.Domain, "", tenantId);
            //AddSettingIfNotExists(EmailSettingNames.Smtp.Port, "994", tenantId); // 465/994
            //AddSettingIfNotExists(EmailSettingNames.Smtp.EnableSsl, "true", tenantId);
            //AddSettingIfNotExists(EmailSettingNames.Smtp.UseDefaultCredentials, "false", tenantId);

            // Languages
            AddSettingIfNotExists(LocalizationSettingNames.DefaultLanguage, "zh-CN", tenantId);
        }

        private void AddSettingIfNotExists(string name, string value, int? tenantId = null)
        {
            if (_context.Settings.IgnoreQueryFilters().Any(s => s.Name == name && s.TenantId == tenantId && s.UserId == null))
            {
                return;
            }

            _context.Settings.Add(new Setting(tenantId, null, name, value));
            _context.SaveChanges();
        }
    }
}
