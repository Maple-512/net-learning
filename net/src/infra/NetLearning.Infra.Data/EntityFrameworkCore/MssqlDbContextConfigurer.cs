namespace Infra.Data.EntityFrameworkCore
{
    using System.Data.Common;
    using Microsoft.EntityFrameworkCore;

    public static class MssqlDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<MssqlDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<MssqlDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}
