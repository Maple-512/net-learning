namespace Infra.Data.EntityFrameworkCore
{
    using Abp.Zero.EntityFrameworkCore;
    using EntityTypeConfigurations;
    using Microsoft.EntityFrameworkCore;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Blogs.Entities;
    using Domain.Core.Identities.Entities;
    using Infra.Data.EntityFrameworkCore.Extensions;

    public class MssqlDbContext : AbpZeroDbContext<Tenant, Role, User, MssqlDbContext>
    {
        /* Define a DbSet for each entity of the application */

        #region BackJob

        public virtual DbSet<HangfireCounter> HangfierCounters { get; set; }
        public virtual DbSet<HangfireJobParameter> HangfireJobParameters { get; set; }
        public virtual DbSet<HangfireJobQueue> HangfireJobQueues { get; set; }
        public virtual DbSet<HangfireList> HangfireLists { get; set; }
        public virtual DbSet<HangfireSchema> HangfireSchemas { get; set; }
        public virtual DbSet<HangfireServer> HangfireServers { get; set; }
        public virtual DbSet<HangfireSet> HangfireSets { get; set; }
        public virtual DbSet<HangfireState> HangfireStates { get; set; }
        public virtual DbSet<HangfireJob> HangfireJobs { get; set; }
        public virtual DbSet<HangfireAggregatedCounter> HangfireAggregatedCounters { get; set; }
        public virtual DbSet<HangfierHash> HangfierHashes { get; set; }

        #endregion

        #region Blog

        public virtual DbSet<Blog> Blogs { get; set; }
        public virtual DbSet<Article> Articles { get; set; }
        public virtual DbSet<BlogTopic> BlogTopics { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Topic> Topics { get; set; }

        #endregion

        #region CastleLog

        //public virtual DbSet<LogRecord> LogRecords { get; set; }

        #endregion

        public MssqlDbContext(DbContextOptions<MssqlDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // 清除Abp的表格式
            modelBuilder.ChangeAbpTable();

            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyBlogConfiguration();
        }
    }
}
