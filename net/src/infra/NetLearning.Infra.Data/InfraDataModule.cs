namespace Infra.Data
{
    using System.Collections.Generic;
    using System.Reflection;
    using Abp.Dapper;
    using Abp.EntityFrameworkCore.Configuration;
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Abp.Zero.EntityFrameworkCore;
    using Domain.Core;
    using EntityFrameworkCore;
    using EntityFrameworkCore.Seed;

    [DependsOn(
        typeof(DomainCoreModule)
        , typeof(AbpZeroCoreEntityFrameworkCoreModule)
        , typeof(AbpDapperModule))]

    public class InfraDataModule : AbpModule
    {
        /* Used it tests to skip dbcontext registration, in order to use in-memory database of EF Core */
        public bool SkipDbContextRegistration { get; set; }

        public bool SkipDbSeed { get; set; }

        public override void PreInitialize()
        {
            if (!SkipDbContextRegistration)
            {
                Configuration.Modules.AbpEfCore().AddDbContext<MssqlDbContext>(options =>
                {
                    if (options.ExistingConnection != null)
                    {
                        MssqlDbContextConfigurer.Configure(options.DbContextOptions, options.ExistingConnection);
                    }
                    else
                    {
                        MssqlDbContextConfigurer.Configure(options.DbContextOptions, options.ConnectionString);
                    }
                });
            }
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(InfraDataModule).GetAssembly());

            //这里会自动去扫描程序集中配置好的映射关系
            DapperExtensions.DapperExtensions.SetMappingAssemblies(new List<Assembly> { typeof(InfraDataModule).GetAssembly() });
        }

        public override void PostInitialize()
        {
            if (!SkipDbSeed)
            {
                SeedHelper.SeedHostDb(IocManager);
            }
        }
    }
}
