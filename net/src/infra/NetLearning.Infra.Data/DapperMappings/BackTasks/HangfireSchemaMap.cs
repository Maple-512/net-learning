namespace Infra.Data.DapperMappings.BackTasks
{
    using DapperExtensions.Mapper;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Configurations;

    public class HangfireSchemaMap : ClassMapper<HangfireSchema>
    {
        public HangfireSchemaMap()
        {
            SchemaName = TableSchemas.BackJob;

            TableName = HangfireJob.TableName;

            AutoMap();
        }
    }
}
