namespace Infra.Data.DapperMappings.BackTasks
{
    using DapperExtensions.Mapper;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Configurations;

    public class HangfierHashMap : ClassMapper<HangfierHash>
    {
        public HangfierHashMap()
        {
            SchemaName = TableSchemas.BackJob;

            TableName = HangfireJob.TableName;

            AutoMap();
        }
    }
}
