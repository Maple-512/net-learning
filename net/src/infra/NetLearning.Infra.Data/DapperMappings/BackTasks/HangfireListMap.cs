namespace Infra.Data.DapperMappings.BackTasks
{
    using DapperExtensions.Mapper;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Configurations;

    public class HangfireListMap : ClassMapper<HangfireList>
    {
        public HangfireListMap()
        {
            SchemaName = TableSchemas.BackJob;

            TableName = HangfireJob.TableName;

            AutoMap();
        }
    }
}
