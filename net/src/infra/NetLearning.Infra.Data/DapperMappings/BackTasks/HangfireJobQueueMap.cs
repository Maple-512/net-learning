namespace Infra.Data.DapperMappings.BackTasks
{
    using DapperExtensions.Mapper;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Configurations;

    public class HangfireJobQueueMap : ClassMapper<HangfireJobQueue>
    {
        public HangfireJobQueueMap()
        {
            SchemaName = TableSchemas.BackJob;

            TableName = HangfireJob.TableName;

            AutoMap();
        }
    }
}
