namespace Infra.Data.DapperMappings.BackTasks
{
    using DapperExtensions.Mapper;
    using Domain.Core.BackTasks.Entities;
    using Domain.Core.Configurations;

    public class HangfireJobParameterMap : ClassMapper<HangfireJobParameter>
    {
        public HangfireJobParameterMap()
        {
            SchemaName = TableSchemas.BackJob;

            TableName = HangfireJob.TableName;

            AutoMap();
        }
    }
}
