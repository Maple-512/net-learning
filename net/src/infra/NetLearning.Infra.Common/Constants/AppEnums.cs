namespace Infra.Common.Constants
{
    /// <summary>
    /// 租户有效性验证状态
    /// </summary>
    public enum TenantAvailabilityStateEnum
    {
        /// <summary>
        /// 可用
        /// </summary>
        Available = 1,

        /// <summary>
        /// 无效
        /// </summary>
        InActive,

        /// <summary>
        /// 未发现
        /// </summary>
        NotFound
    }

    /// <summary>
    /// 验证码类型
    /// </summary>
    public enum VerifyCodeTypeEnum : byte
    {
        /// <summary>
        /// 用户注册
        /// </summary>
        RegisterUser,

        /// <summary>
        /// 用户激活
        /// </summary>
        ActivateUser,

        /// <summary>
        /// 用户登录
        /// </summary>
        LoginUser,
    }

    /// <summary>
    /// 评论类型
    /// </summary>
    public enum CommentTypeEnum : byte
    {
        /// <summary>
        /// 评论博客
        /// </summary>
        Blog,

        /// <summary>
        /// 回复评论
        /// </summary>
        Reply,

        /// <summary>
        /// 引用评论
        /// </summary>
        Quote,
    }

    /// <summary>
    /// 博客类型
    /// </summary>
    public enum BlogTypeEnum : byte
    {
        /// <summary>
        /// 原创
        /// </summary>
        Original,

        /// <summary>
        /// 转载
        /// </summary>
        Reprint
    }
}
