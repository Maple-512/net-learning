namespace Infra.Common.Localizations
{
    using Abp.Configuration.Startup;
    using Abp.Localization.Dictionaries;
    using Abp.Reflection.Extensions;
    using Shared.Localization.Yml;

    public class LocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(InfraCommonConfig.LocalizationSourceName,
                    new YmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(LocalizationConfigurer).GetAssembly(),
                        "NetLearning.Infra.Common.Localizations.SourceFiles"
                    )
                )
            );
        }
    }
}
