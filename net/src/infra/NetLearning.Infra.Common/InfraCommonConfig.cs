namespace Infra.Common
{
    /// <summary>
    /// 应用程序配置
    /// </summary>
    public class InfraCommonConfig
    {
        /// <summary>
        /// 是否启用多租户
        /// </summary>
        public static bool MultiTenancyEnabled = true;

        public const string ConnectionStringName = "Default";

        public const string DefaultPassPhrase = "227b0c4357*^940db+69571";

        public const string LocalizationSourceName = "Localization";
    }
}
