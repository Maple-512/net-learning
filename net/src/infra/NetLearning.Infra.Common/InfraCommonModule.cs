namespace Infra.Common
{
    using Abp;
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Infra.Common.Localizations;

    [DependsOn(typeof(AbpKernelModule))]
    public class InfraCommonModule : AbpModule
    {
        public override void PreInitialize()
        {
            LocalizationConfigurer.Configure(Configuration.Localization);

            //Configuration.Settings.Providers.Add<ApplicationSettingProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(InfraCommonModule).GetAssembly());
        }
    }
}
