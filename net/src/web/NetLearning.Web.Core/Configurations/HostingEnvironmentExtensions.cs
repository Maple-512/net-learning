namespace Web.Core.Configurations
{
    using Domain.Core.Configurations;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Hosting;

    public static class HostingEnvironmentExtensions
    {
        public static IConfigurationRoot GetAppConfiguration(this IWebHostEnvironment env)
        {
            return AppConfiguration.Get(env.ContentRootPath, env.EnvironmentName, env.IsDevelopment());
        }
    }
}
