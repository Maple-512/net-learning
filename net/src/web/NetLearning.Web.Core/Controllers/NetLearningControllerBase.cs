namespace Web.Core.Controllers
{
    using Abp.AspNetCore.Mvc.Controllers;
    using Abp.IdentityFramework;
    using Infra.Common;
    using Microsoft.AspNetCore.Identity;

    public abstract class NetLearningControllerBase : AbpController
    {
        protected NetLearningControllerBase()
        {
            LocalizationSourceName = InfraCommonConfig.LocalizationSourceName;
        }

        protected void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}
