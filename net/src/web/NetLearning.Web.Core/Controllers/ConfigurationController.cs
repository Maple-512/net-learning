﻿namespace Web.Core.Controllers
{
    using System.Threading.Tasks;
    using Abp.Web.Configuration;
    using Microsoft.AspNetCore.Mvc;

    public class ConfigurationController : NetLearningControllerBase
    {
        private readonly AbpUserConfigurationBuilder _abpUserConfigurationBuilder;

        public ConfigurationController(AbpUserConfigurationBuilder abpUserConfigurationBuilder)
        {
            _abpUserConfigurationBuilder = abpUserConfigurationBuilder;
        }

        [HttpGet]
        public async Task<JsonResult> GetAll()
        {
            var userConfig = await _abpUserConfigurationBuilder.GetAll();

            return Json(userConfig);
        }
    }
}
