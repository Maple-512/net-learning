namespace Web.Core
{
    using System;
    using System.Text;
    using Abp.AspNetCore;
    using Abp.AspNetCore.Configuration;
    using Abp.AspNetCore.SignalR;
    using Abp.Configuration.Startup;
    using Abp.Hangfire;
    using Abp.Hangfire.Configuration;
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Abp.Runtime.Caching.Redis;
    using Abp.Zero.Configuration;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.IdentityModel.Tokens;
    using Application.Core;
    using Infra.Common;
    using Infra.Data;
    using Web.Core.Authentication.JwtBearer;
    using Web.Core.Configurations;

    [DependsOn(
         typeof(ApplicationCoreModule)
        , typeof(InfraDataModule)
        , typeof(AbpAspNetCoreModule)
        , typeof(AbpAspNetCoreSignalRModule)
        , typeof(AbpRedisCacheModule)
        , typeof(AbpHangfireAspNetCoreModule)
     )]
    public class WebCoreModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;

        private readonly IConfigurationRoot _appConfiguration;

        public WebCoreModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                InfraCommonConfig.ConnectionStringName
            );

            Configuration.Modules.AbpWebCommon().SendAllExceptionsToClients = true;

            // Use database for language management
            Configuration.Modules.Zero().LanguageManagement.EnableDbLocalization();

            ConfigureTokenAuth();

            Configuration.BackgroundJobs.UseHangfire();

            // Redis
            Configuration.Caching.UseRedis();

            CreateControllers();
        }

        private void ConfigureTokenAuth()
        {
            IocManager.Register<TokenAuthConfiguration>();

            var tokenAuthConfig = IocManager.Resolve<TokenAuthConfiguration>();

            tokenAuthConfig.SecurityKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_appConfiguration["Authentication:JwtBearer:SecurityKey"]));

            tokenAuthConfig.Issuer = _appConfiguration["Authentication:JwtBearer:Issuer"];

            tokenAuthConfig.Audience = _appConfiguration["Authentication:JwtBearer:Audience"];

            tokenAuthConfig.SigningCredentials = new SigningCredentials(tokenAuthConfig.SecurityKey, SecurityAlgorithms.HmacSha256);

            tokenAuthConfig.Expiration = TimeSpan.FromDays(1);
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebCoreModule).GetAssembly());
        }

        private void CreateControllers()
        {
            Configuration.Modules.AbpAspNetCore()
                 .CreateControllersForAppServices(
                     typeof(ApplicationCoreModule).GetAssembly()
                 );
        }
    }
}
