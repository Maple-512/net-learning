﻿namespace Web.Core.Models.TokenAuth
{
    using Abp.AutoMapper;
    using Web.Core.Authentication.External;

    [AutoMapFrom(typeof(ExternalLoginProviderInfo))]
    public class ExternalLoginProviderInfoModel
    {
        public string Name { get; set; }

        public string ClientId { get; set; }
    }
}
