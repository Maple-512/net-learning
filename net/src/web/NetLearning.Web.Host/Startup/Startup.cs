namespace Web.Host.Startup
{
    using System;
    using System.Linq;
    using System.Reflection;
    using Abp.AspNetCore;
    using Abp.AspNetCore.Mvc.Antiforgery;
    using Abp.AspNetCore.SignalR.Hubs;
    using Abp.Dependency;
    using Abp.Extensions;
    using Abp.Json;
    using Castle.Facilities.Logging;
    using Core.Authentication;
    using Core.Configurations;
    using Domain.Core.Configurations;
    using Domain.Core.Identities;
    using Hangfire;
    using Hangfire.SqlServer;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Hosting;
    using Microsoft.OpenApi.Models;
    using Newtonsoft.Json.Serialization;
    using Shared.Nlog;

    public class Startup
    {
        private const string DefaultCorsPolicyName = "localhost";

        private readonly IConfigurationRoot _configuration;

        private readonly IWebHostEnvironment _env;

        public Startup(IWebHostEnvironment env)
        {
            _env = env;
            _configuration = _env.GetAppConfiguration();
        }

        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //MVC
            services.AddControllersWithViews(options =>
            {
                options.Filters.Add(new AbpAutoValidateAntiforgeryTokenAttribute());
            })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new AbpMvcContractResolver(IocManager.Instance)
                    {
                        NamingStrategy = new CamelCaseNamingStrategy()
                    };
                });

            IdentityRegistrar.Register(services);

            AuthenticationConfigurer.Configure(services, _configuration);

            services.AddSignalR();

            // Configure CORS for angular UI
            services.AddCors(options =>
            {
                options.AddPolicy(
                    DefaultCorsPolicyName,
                    builder => builder
                        .WithOrigins(
                            // App:CorsOrigins in appsettings.json can contain more than one address separated by comma.
                            _configuration["App:CorsOrigins"]
                                .Split(",", StringSplitOptions.RemoveEmptyEntries)
                                .Select(o => o.RemovePostFix("/"))
                                .ToArray()
                        )
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials()
                );
            });

            if (_env.IsDevelopment())
            {
                // Swagger - Enable this line and the related lines in Configure method to enable swagger UI
                services.AddSwaggerGen(options =>
                {
                    options.SwaggerDoc("v1", new OpenApiInfo() { Title = "NetLearning API", Version = "v1" });

                    options.DocInclusionPredicate((docName, description) => true);

                    // Define the BearerAuth scheme that's in use
                    options.AddSecurityDefinition("bearerAuth", new OpenApiSecurityScheme()
                    {
                        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.ApiKey
                    });
                });
            }

            // Hangfire
            services.AddHangfire(config =>
            {
                var options = new SqlServerStorageOptions()
                {
                    SchemaName = TableSchemas.BackJob,
                };

                config.UseSqlServerStorage(_configuration.GetConnectionString("Default"), options);
            });

            // Configure Abp and Dependency Injection
            return services.AddAbp<WebHostModule>(options =>
            {
                // Configure Log4Net logging
                options.IocManager.IocContainer.AddFacility<LoggingFacility>(f =>
                {
                    f.UseNLog().WithConfig("nlog.config");
                });
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            // Initializes ABP framework.
            app.UseAbp(options => { options.UseAbpRequestLocalization = false; });

            // Enable CORS!
            app.UseCors(DefaultCorsPolicyName);

            app.UseRouting();

            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<AbpCommonHub>("/signalr");
                endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}");
            });

            app.UseHangfireServer();

            app.UseSwagger();

            if (_env.IsDevelopment())
            {
                app.UseHangfireDashboard()
                .UseSwaggerUI(options =>
                {
                    options.SwaggerEndpoint(_configuration["App:ServerRootAddress"].EnsureEndsWith('/') + "swagger/v1/swagger.json", "NetLearning API V1");

                    options.IndexStream = () => Assembly.GetExecutingAssembly()
                        .GetManifestResourceStream("NetLearning.Web.Host.wwwroot.swagger.ui.index.html");
                });
            }
        }
    }
}
