namespace Web.Host
{
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Core;
    using Core.Configurations;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.Extensions.Configuration;

    [DependsOn(
       typeof(WebCoreModule))]
    public class WebHostModule : AbpModule
    {
        private readonly IWebHostEnvironment _env;

        private readonly IConfigurationRoot _appConfiguration;

        public WebHostModule(IWebHostEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(WebHostModule).GetAssembly());
        }
    }
}
