namespace Web.Host.Controllers
{
    using Core.Controllers;
    using Microsoft.AspNetCore.Antiforgery;

    public class AntiForgeryController : NetLearningControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
