namespace Tool.Migrator.DependencyInjection
{
    using Abp.Dependency;
    using Castle.Windsor.MsDependencyInjection;
    using Domain.Core.Identities;
    using Microsoft.Extensions.DependencyInjection;

    public static class ServiceCollectionRegistrar
    {
        public static void Register(IIocManager iocManager)
        {
            var services = new ServiceCollection();

            IdentityRegistrar.Register(services);

            WindsorRegistrationHelper.CreateServiceProvider(iocManager.IocContainer, services);
        }
    }
}
