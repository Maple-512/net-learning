namespace Tool.Migrator
{
    using Abp.Events.Bus;
    using Abp.Modules;
    using Abp.Reflection.Extensions;
    using Castle.MicroKernel.Registration;
    using Domain.Core.Configurations;
    using Infra.Common;
    using Infra.Data;
    using Microsoft.Extensions.Configuration;
    using Tool.Migrator.DependencyInjection;

    [DependsOn(typeof(InfraDataModule))]
    public class ToolMigratorModule : AbpModule
    {
        private readonly IConfigurationRoot _appConfiguration;

        public ToolMigratorModule(InfraDataModule abpProjectNameEntityFrameworkModule)
        {
            abpProjectNameEntityFrameworkModule.SkipDbSeed = true;

            _appConfiguration = AppConfiguration.Get(
                typeof(InfraDataModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }

        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = _appConfiguration.GetConnectionString(
                InfraCommonConfig.ConnectionStringName
            );

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;

            Configuration.ReplaceService(
                typeof(IEventBus),
                () => IocManager.IocContainer.Register(
                    Component.For<IEventBus>().Instance(NullEventBus.Instance)
                )
            );
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(ToolMigratorModule).GetAssembly());

            ServiceCollectionRegistrar.Register(IocManager);
        }
    }
}
